// Defined the type of actions
const SHOW_PRODUCT = 'SHOW_PRODUCT';

// Defined the actions
const updateProductList = data => dispatch => {
  dispatch({
    type: SHOW_PRODUCT,
    payload: {
      data: data,
      timeAt: new Date().toLocaleString
    }
  });
};

export { SHOW_PRODUCT, updateProductList };
