import { combineReducers } from 'redux';
import { productsReducer } from './products';

// The store will be saved `products` as an item in the storage
const allReducer = combineReducers({
  products: productsReducer
});

export default allReducer;
