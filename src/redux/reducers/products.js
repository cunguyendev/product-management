import { SHOW_PRODUCT } from '../actions/productActions';

// The first data for init
// Should do like this for more professional
const initialProductState = {
  data: [],
  timeAt: new Date().toLocaleString()
};

// Validation for each actions get from user
export const productsReducer = (state = initialProductState, action) => {
  switch (action.type) {
    case SHOW_PRODUCT:
      return action.payload;

    default:
      return state;
  }
};
