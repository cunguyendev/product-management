/**
 * This function to check the value is object or not
 * @param {*} value
 */
const isObject = value => {
  return value && typeof value === 'object' && value.constructor === Object;
};

/**
 * This function for converting from object to
 * array if the data from argument is object
 * @param {*} object
 */
const convertToArray = object => {
  if (object) {
    if (isObject(object)) {
      let data = [];
      Object.keys(object).map(key => (data[key] = object[key]));

      return data;
    } else {
      return object;
    }
  }
};

export { isObject, convertToArray };
