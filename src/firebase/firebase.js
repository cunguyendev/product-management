import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID
};

const database = {
  products: 'products',
  categories: 'categories'
};

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.database = app.database();
  }

  getProducts = () => this.database.ref(`${database.products}`);
  getCategories = () => this.database.ref(`${database.categories}`);
  queryProducts = index => this.database.ref(`${database.products}/${index}`);
}

export default Firebase;
