import React, { useEffect, useState, useRef } from 'react';
import { withFirebase } from './firebase';
import { connect } from 'react-redux';
import { convertToArray } from './utility/helper';
import { updateProductList } from './redux/actions/productActions';

function App({ firebase, updateProductList, products }) {
  const [formStatus, setFormStatus] = useState(false);
  const nameRef = useRef(null);
  const quantityRef = useRef(null);
  const priceRef = useRef(null);
  const categoryRef = useRef(null);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    // getProducts from firebase.js
    firebase.getProducts().on('value', snapshot => {
      // Check data object or array
      // The data will be converted if it is object
      // After that update to store of Redux
      updateProductList(convertToArray(snapshot.val()));
    });

    firebase.getCategories().on('value', snapshot => {
      // Check data object or array
      // The data will be converted if it is object
      // After that update to state
      setCategories(convertToArray(snapshot.val()));
    });
  }, [firebase, updateProductList]);

  const handleRemove = id => {
    firebase.queryProducts(id).remove();
  };

  const handleEdit = id => {
    setFormStatus({
      status: true,
      title: 'Edit',
      data: { id: id, productData: products.data[id] }
    });
  };

  const productItem = products.data.map((item, index) => (
    <li key={index}>
      {item.name} <button onClick={() => handleEdit(index)}>Edit</button>
      <button onClick={() => handleRemove(index)}>Remove</button>{' '}
    </li>
  ));

  const categoryItem = categories.map((item, index) => (
    <option key={index} value={index}>
      {item.name}
    </option>
  ));

  const openForm = (status, title, data) => {
    setFormStatus({ status: status, title: title, data: data });
  };

  const closeForm = () => {
    setFormStatus({ status: false });
  };

  const submit = () => {
    switch (formStatus.title) {
      case 'Add':
        firebase
          .queryProducts(products.data.length)
          .set({
            name: nameRef.current.value,
            quantity: quantityRef.current.value,
            price: priceRef.current.value,
            categoryId: categoryRef.current.value
          })
          .then(() => {
            closeForm();
          });

        break;

      case 'Edit':
        firebase
        .queryProducts(formStatus.data.id)
        .set({
          name: nameRef.current.value,
          quantity: quantityRef.current.value,
          price: priceRef.current.value,
          categoryId: categoryRef.current.value
        })
        .then(() => {
          closeForm();
        });

        break;

      default:
        break;
    }
  };

  return (
    <div className="App">
      <button onClick={() => openForm(true, 'Add', [])}>Add</button>
      <hr />
      {formStatus.status && (
        <div>
          <input
            defaultValue={
              formStatus.title === 'Edit' ? formStatus.data.productData.name : ''
            }
            ref={nameRef}
            type="text"
            placeholder="Input name"
          />
          <input
            defaultValue={
              formStatus.title === 'Edit' ? formStatus.data.productData.quantity : ''
            }
            ref={quantityRef}
            type="text"
            placeholder="Input quantity"
          />
          <select ref={categoryRef}>{categoryItem}</select>
          <input
            defaultValue={
              formStatus.title === 'Edit' ? formStatus.data.productData.price : ''
            }
            ref={priceRef}
            type="text"
            placeholder="Input price"
          />
          <button onClick={() => submit()}>Save</button>
          <button onClick={() => closeForm()}>Close</button>
        </div>
      )}
      <hr />
      <ul>{productItem}</ul>
    </div>
  );
}

const mapStateToProps = state => ({
  products: state.products
});

const mapActionsToProps = {
  updateProductList
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withFirebase(App));
