# Product Management

## Requirements

- yarn installed on Mac, Ubuntu, Windows

## Getting Started

- **Clone this repo**

```
git clone https://gitlab.com/cunguyendev/product-management
```

- **Go to directory root**

```
cd product-management
```

- **Install packages**

```
yarn install
```

- **Start server**

```
yarn start
```

- **Open on web**

```
http://localhost:3000
```

- **Notes:**
  - Live-reload is supported
